import React from "react";
import Navigation from "./Navigation.js";
import Search from "./Search.js";
import Logo from "./Logo.js";
import UserDropdown from "./UserDropdown.js";

function Header() {
  const activeStyle = {
    backgroundColor: "#198593",
    boxShadow: "inset 0 0 6px rgba(0,0,0,0.2)"
  };

  return (
    <div className="row header">
      <div className="col-md-2 align-self-center logo">
        <Logo />
      </div>
      <div className="col-md-5 align-self-center nav-div">
        <div className="nav-short">
          <Navigation activeStyle={activeStyle} />
        </div>
      </div>
      <div className="col-md-2 align-self-center">
        <Search />
      </div>
      <div className="col-md-2 align-self-top user">
        <UserDropdown />
      </div>
    </div>
  );
}

export default Header;
