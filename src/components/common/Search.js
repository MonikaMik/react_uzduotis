import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Search() {
  return (
    <div className="input-group no-pad align-self-center search borders">
      <input
        type="text"
        className="form-control round-border-left"
        placeholder="Search"
        aria-describedby="basic-addon2"
      />
      <div className="input-group-append">
        <span className="input-group-text round-border-right" id="basic-addon2">
          <FontAwesomeIcon icon="search" />
        </span>
      </div>
    </div>
  );
}

export default Search;
