import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Dropdown from "react-bootstrap/Dropdown";

const UserDropdown = props => {
  return (
    <Dropdown alignRight>
      <Dropdown.Toggle id="dropdown-menu-align-right">
        <FontAwesomeIcon className="user-dropdown-icon" icon="user" />
        <span className="username">Jorah Mormont </span>
      </Dropdown.Toggle>
      <Dropdown.Menu className="user-dropdown-menu">
        <Dropdown.Item className="user-dropdown" eventKey="1">
          <FontAwesomeIcon className="user-dropdown-icon" icon="users" />
          Groups
        </Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item className="user-dropdown" eventKey="2">
          <FontAwesomeIcon className="user-dropdown-icon" icon="comments" />
          Frequently contacted
        </Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item className="user-dropdown" eventKey="3" active>
          <FontAwesomeIcon className="user-dropdown-icon" icon="wrench" />
          Preferences
        </Dropdown.Item>
        <Dropdown.Divider />
        <Dropdown.Item className="user-dropdown" eventKey="4">
          <FontAwesomeIcon className="user-dropdown-icon" icon="power-off" />
          Log out
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};

export default UserDropdown;
