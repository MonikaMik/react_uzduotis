import React from "react";

function TextInput(props) {
  return (
    <div className="form-group no-pad align-self-center borders">
      <input
        id={props.id}
        type="text"
        onChange={props.onChange}
        name={props.name}
        className="form-control round-border-left round-border-right"
        placeholder={props.placeholder}
        value={props.value}
      />
    </div>
  );
}

export default TextInput;
