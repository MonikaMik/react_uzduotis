import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Footer() {
  return (
    <footer className="footer">
      <div className="row">
        <div className="col-sm-5 footer-section">
          <ul className="nav-list">
            <li className="nav-li">
              <a className="bottom-nav" href="/">
                Dashboard
              </a>
            </li>
            <li className="nav-li">
              <a className="bottom-nav" href="/">
                Contacts
              </a>
            </li>
            <li className="nav-li">
              <a className="bottom-nav" href="/">
                Notification
              </a>
            </li>
          </ul>
          <p>
            &#169; Contactify 2015{" "}
            <a className="footer-link" href="/">
              About
            </a>{" "}
            <a className="footer-link" href="/">
              Privacy
            </a>
          </p>
        </div>

        <div className="col-sm-5 footer-half-panel">
          <div className="row sync">
            <div className="col-sm-2 d-flex align-items-center justify-content-center">
              <FontAwesomeIcon size="3x" icon="cloud-upload-alt" />
            </div>
            <div className="col-sm-7 d-flex no-pad justify-content-center flex-column">
              <div>Last synced:</div>
              <div>2015-06-02 14:33:10</div>
            </div>
            <div className="col-sm-3 d-flex align-items-center">
              <FontAwesomeIcon icon="sync-alt" />
              <a href="/" className="space">
                Force sync
              </a>
            </div>
          </div>
          <div className="row info">
            <div className="col-sm-2 d-flex align-items-center justify-content-center">
              <FontAwesomeIcon size="3x" icon="stethoscope" />
            </div>
            <div className="col-sm-10 d-flex no-pad justify-content-center flex-column">
              <div>Help desk and Resolution center available:</div>
              <div>I-IV 8:00-18:00, V 8:00-16:45</div>
            </div>
          </div>
        </div>

        <div className="col-sm-2 footer-section">
          <ul className="nav-list">
            <li className="nav-li">
              <a className="bottom-nav" href="/">
                Groups
              </a>
            </li>
            <li className="nav-li">
              <a className="bottom-nav" href="/">
                Frequently contacted
              </a>
            </li>
            <li className="nav-li">
              <a className="bottom-nav" href="/">
                Preferences
              </a>
            </li>
            <li className="nav-li">
              <a className="bottom-nav" href="/">
                Log out
              </a>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
