import React from "react";
import { NavLink } from "react-router-dom";

const Navigation = props => {
  return (
    <nav className="nav">
      <NavLink
        className="col nav-link"
        to="/dashboard"
        activeStyle={props.activeStyle}
      >
        DASHBOARD
      </NavLink>
      <NavLink
        className="col nav-link side-border"
        exact
        to="/"
        activeStyle={props.activeStyle}
      >
        CONTACTS
      </NavLink>
      <NavLink
        className="col nav-link"
        to="/notifications"
        activeStyle={props.activeStyle}
      >
        NOTIFICATIONS
      </NavLink>
    </nav>
  );
};

export default Navigation;
