import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function AddNewContact(props) {
  return (
    <button className="add-button float-right">
      <FontAwesomeIcon
        className="circle-icon"
        color="#FFF"
        size="2x"
        icon="plus"
      />{" "}
      &nbsp; &nbsp; &nbsp; &nbsp; ADD NEW CONTACT
    </button>
  );
}

export default AddNewContact;
