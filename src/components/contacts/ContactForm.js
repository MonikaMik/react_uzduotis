import React from "react";
import TextInput from "../common/TextInput";
import "../../App.css";

function ContactForm(props) {
  return (
    <div className="row create-form">
      <div className="col-sm-4">
        <h3>Contact information</h3>
        <br />
        <form onSubmit={props.onSubmit}>
          <TextInput
            id="name"
            label="name"
            onChange={props.onChange}
            name="name"
            value={props.contact.name}
            placeholder="Name"
          />
          <br />
          <TextInput
            id="surname"
            label="surname"
            onChange={props.onChange}
            name="surname"
            value={props.contact.surname}
            placeholder="Surname"
          />
          <br />
          <TextInput
            id="city"
            label="city"
            onChange={props.onChange}
            name="city"
            value={props.contact.city}
            placeholder="City"
          />
          <br />
          <TextInput
            id="email"
            label="email"
            onChange={props.onChange}
            name="email"
            value={props.contact.email}
            placeholder="Email"
          />
          <br />
          <TextInput
            id="phone"
            label="phone"
            name="phone"
            onChange={props.onChange}
            value={props.contact.phone}
            placeholder="Phone"
          />
          <br />
          <input type="submit" value="Save" className="btn btn-primary" />
        </form>
      </div>
    </div>
  );
}

export default ContactForm;
