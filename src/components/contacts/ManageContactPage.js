import React, { useState, useEffect } from "react";
import ContactForm from "./ContactForm";
import * as contactsApi from "../../api/contactsApi";

const ManageContactPage = props => {
  const [contact, setContact] = useState({
    id: null,
    name: "",
    surname: "",
    city: "",
    email: "",
    phone: "",
    active: false
  });

  useEffect(() => {
    const id = props.match.params.id;
    if (id) {
      contactsApi.getContactById(id).then(_contact => setContact(_contact));
    }
  }, [props.match.params.id]);

  function handleChange({ target }) {
    setContact({
      ...contact,
      [target.name]: target.value
    });
  }

  function handleSumbit(event) {
    event.preventDefault();
    contactsApi.saveContact(contact).then(() => {
      props.history.push("/");
    });
  }

  return (
    <>
      <ContactForm
        contact={contact}
        onChange={handleChange}
        onSubmit={handleSumbit}
      />
    </>
  );
};

export default ManageContactPage;
