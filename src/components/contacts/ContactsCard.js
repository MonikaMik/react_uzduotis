import React from "react";
import userPic from "./userpic.jpg";

function ContactsCard(props) {
  return (
    <div className="card contact-card">
      <div className="card-body">
        <img
          src={userPic}
          className="rounded-circle avatar"
          alt={props.selectedContact.name}
        />
        <div className="row userInfo">
          <div className="col-sm-4 text-right category">Name:</div>
          <div className="col-sm-8">{props.selectedContact.name}</div>

          <div className="col-sm-4 text-right category">Surame:</div>
          <div className="col-sm-8">{props.selectedContact.surname}</div>

          <div className="col-sm-4 text-right category">City:</div>
          <div className="col-sm-8">{props.selectedContact.city}</div>

          <div className="col-sm-4 text-right category">Email:</div>
          <div className="col-sm-8">
            <a href={props.selectedContact.email}>
              {props.selectedContact.email}
            </a>
          </div>

          <div className="col-sm-4 text-right category">Phone:</div>
          <div className="col-sm-8">{props.selectedContact.phone}</div>
        </div>
      </div>
    </div>
  );
}

export default ContactsCard;
