import React, { useState, useEffect } from "react";
import * as contactsApi from "../../api/contactsApi";
import ContactsList from "./ContactsList.js";
import ContactsCard from "./ContactsCard.js";
import Filters from "./Filters.js";
import AddNewContact from "./NewContact.js";
import { Link } from "react-router-dom";

function ContactsPage(props) {
  const [contacts, setContacts] = useState([]);
  const [selectedContact, setSelectedContact] = useState({});
  const [filters, setFilters] = useState({
    name: "",
    city: "",
    active: false
  });
  const [filtered, setFiltered] = useState(false);

  function selectContact(contact) {
    setSelectedContact(contact);
  }

  useEffect(() => {
    contactsApi.getContacts().then(_contacts => setContacts(_contacts));
  }, [filtered]);

  function deleteContact(contactid) {
    const arrayCopy = contacts.filter(contact => contact.id !== contactid);
    contactsApi
      .deleteContact(contactid)
      .then(sortedContacts => setContacts(arrayCopy));
  }

  function handleFilterChange({ target }) {
    if (filtered === true) {
      setFiltered(false);
    } else {
      setFiltered(true);
    }

    const value = target.type === "checkbox" ? target.checked : target.value;
    setFilters({
      ...filters,
      [target.name]: value
    });
  }

  function handleFilterSumbit(event) {
    event.preventDefault();

    let arrayCopy = contacts;

    if (event.target.name.value !== "") {
      arrayCopy = arrayCopy.filter(
        contact => contact.name === event.target.name.value
      );
    }
    if (event.target.city.value !== "") {
      arrayCopy = arrayCopy.filter(
        contact => contact.city === event.target.city.value
      );
    }
    if (event.target.active.checked === true) {
      arrayCopy = arrayCopy.filter(contact => contact.active === true);
    }
    setContacts(arrayCopy);
  }

  return (
    <>
      <div className="row filters">
        <div className="col-sm-7 align-self-center">
          <Filters
            contacts={contacts}
            filters={filters}
            onChange={handleFilterChange}
            onSubmit={handleFilterSumbit}
          />
        </div>
        <div className="col-sm-3 align-self-center ml-auto">
          <Link to="/contact">
            <AddNewContact />
          </Link>
        </div>
      </div>
      <div className="d-flex flex-row justify-content-between my-flex-container contacts-page">
        <div className="my-flex-item p-3">
          <ContactsCard selectedContact={selectedContact} />
        </div>

        <div className="my-flex-item p-3 flex-fill">
          <ContactsList
            contacts={contacts}
            selectContact={selectContact}
            deleteContact={deleteContact}
            selectedContact={selectedContact}
          />
        </div>
      </div>
    </>
  );
}

export default ContactsPage;
