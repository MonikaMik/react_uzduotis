import React, { useState } from "react";
import "../../App.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Table from "react-bootstrap/Table";
import { Link } from "react-router-dom";
import { ReactComponent as Pencil } from "./pencil-blue.svg";
import { ReactComponent as Trash } from "./trash-blue.svg";

function ContactsList(props) {
  let [copy, setCopy] = useState([]);
  let [sortedAsc, setSortedAsc] = useState(false);
  let [sortedDesc, setSortedDesc] = useState(false);

  copy = props.contacts;

  const compareBy = key => {
    return function(a, b) {
      if (a[key] < b[key]) return -1;
      if (a[key] > b[key]) return 1;
      return 0;
    };
  };

  const compareByDesc = key => {
    return function(a, b) {
      if (a[key] < b[key]) return 1;
      if (a[key] > b[key]) return -1;
      return 0;
    };
  };

  function sortBy(key) {
    setSortedAsc(true);
    setSortedDesc(false);
    const lala = copy.sort(compareBy(key));
    setCopy(prevState => {
      return { ...prevState, copy: lala };
    });
  }

  function unsortBy(key) {
    setSortedAsc(false);
    setSortedDesc(true);
    const lala = copy.sort(compareByDesc(key));
    setCopy(prevState => {
      return { ...prevState, copy: lala };
    });
  }

  let icon;

  if (sortedAsc) {
    icon = (
      <FontAwesomeIcon
        onClick={() => unsortBy("name")}
        className="float-right name-arrow"
        icon="arrow-up"
      />
    );
  } else {
    icon = (
      <FontAwesomeIcon
        className="float-right name-arrow"
        onClick={() => sortBy("name")}
        icon="arrow-down"
      />
    );
  }

  return (
    <div className="panel panel-default contacts-panel">
      <Table hover className="table">
        <thead>
          <tr>
            <th className="active" />
            <th className="name">
              Name
              {icon}
            </th>
            <th className="surname vertical-separator">Surname</th>
            <th className="city vertical-separator">City</th>
            <th className="email vertical-separator">Email</th>
            <th className="phone vertical-separator">Phone</th>
            <th className="actions vertical-separator" />
          </tr>
        </thead>
        <tbody>
          {copy.map(contact => {
            if (contact.id === props.selectedContact.id) {
              debugger;
              return (
                <tr
                  className="selected"
                  key={contact.id}
                  data-item={contact}
                  onClick={() => {
                    props.selectContact(contact);
                  }}
                >
                  <td key={contact.id} className="centered">
                    <ActiveIcon active={contact.active} />
                  </td>
                  <td>{contact.name}</td>
                  <td>{contact.surname}</td>
                  <td>{contact.city}</td>
                  <td>{contact.email}</td>
                  <td className="contact-phone">{contact.phone}</td>
                  <td className="centered">
                    <Link to={"/contact/" + contact.id}>
                      <Pencil className="action-icon-selected" />
                    </Link>

                    <Trash
                      className="action-icon-selected"
                      onClick={() => {
                        props.deleteContact(contact.id);
                      }}
                    />
                  </td>
                </tr>
              );
            } else {
              debugger;
              return (
                <tr
                  key={contact.id}
                  data-item={contact}
                  onClick={() => {
                    props.selectContact(contact);
                  }}
                >
                  <td key={contact.id} className="centered">
                    <ActiveIcon active={contact.active} />
                  </td>
                  <td>{contact.name}</td>
                  <td>{contact.surname}</td>
                  <td>{contact.city}</td>
                  <td>{contact.email}</td>
                  <td className="contact-phone">{contact.phone}</td>
                  <td className="centered">
                    <Link to={"/contact/" + contact.id}>
                      <FontAwesomeIcon
                        className="action-icon"
                        icon="pencil-alt"
                      />
                    </Link>
                    <FontAwesomeIcon
                      className="action-icon"
                      icon="trash-alt"
                      onClick={() => {
                        props.deleteContact(contact.id);
                      }}
                    />
                  </td>
                </tr>
              );
            }
          })}
        </tbody>
      </Table>
    </div>
  );
}

function ActiveIcon(props) {
  const isActive = props.active;
  if (isActive) {
    return <FontAwesomeIcon icon={["far", "eye"]} />;
  }
  return <FontAwesomeIcon icon={["far", "eye-slash"]} />;
}

export default ContactsList;
