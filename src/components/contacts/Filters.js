import React from "react";
import TextInput from "../common/TextInput.js";

function Filters(props) {
  const nuDuplicates = [
    ...new Map(props.contacts.map(o => [o.city, o])).values()
  ];

  return (
    <>
      <form onSubmit={props.onSubmit}>
        <div className="row">
          <div className="col-4">
            <TextInput
              onChange={props.onChange}
              name="name"
              placeholder="Name"
              value={props.filters.name}
            />
          </div>
          <div className="col-3">
            <div className="form-group no-pad align-self-center borders">
              <select
                name="city"
                onChange={props.onChange}
                value={props.filters.city || ""}
                className="custom-select round-border-left round-border-right"
              >
                <option value="">City</option>
                {nuDuplicates.map(contact => {
                  return (
                    <option key={contact.id} value={contact.city}>
                      {contact.city}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>
          <div className="col-2 align-self-center">
            <div className="form-check">
              <input
                name="active"
                type="checkbox"
                checked={props.active}
                onChange={props.onChange}
                className="form-check-input check-borders"
                id="defaultCheck1"
              />
              <label className="form-check-label" htmlFor="defaultCheck1">
                Show active
              </label>
            </div>
          </div>
          <div className="col-2">
            <input type="submit" className="filter-button" value="FILTER" />
          </div>
        </div>
      </form>
    </>
  );
}

export default Filters;
