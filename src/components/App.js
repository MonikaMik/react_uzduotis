import React from "react";
import { Route } from "react-router-dom";
import ContactsPage from "./contacts/ContactsPage";
import DashboardPage from "./dashboard/DashboardPage";
import NotificationsPage from "./notifications/NotificationsPage";
import Header from "./common/Header";
import Footer from "./common/Footer";
import { library } from "@fortawesome/fontawesome-svg-core";
import ManageContactPage from "./contacts/ManageContactPage";
import {
  faSearch,
  faUser,
  faPlus,
  faCircle,
  faArrowDown,
  faArrowUp,
  faPencilAlt,
  faTrashAlt,
  faCloudUploadAlt,
  faStethoscope,
  faSyncAlt,
  faUsers,
  faComments,
  faWrench,
  faPowerOff
} from "@fortawesome/free-solid-svg-icons";
import { faEye, faEyeSlash } from "@fortawesome/free-regular-svg-icons";

library.add(
  faSearch,
  faUser,
  faPlus,
  faCircle,
  faArrowDown,
  faArrowUp,
  faPencilAlt,
  faTrashAlt,
  faCloudUploadAlt,
  faStethoscope,
  faSyncAlt,
  faEye,
  faEyeSlash,
  faUsers,
  faComments,
  faWrench,
  faPowerOff
);

function App() {
  return (
    <div className="container-fluid">
      <Header />
      <Route path="/dashboard" component={DashboardPage} />
      <Route exact path="/" component={ContactsPage} />
      <Route path="/notifications" component={NotificationsPage} />
      <Route path="/contact/:id" component={ManageContactPage} />
      <Route exact path="/contact" component={ManageContactPage} />
      <Footer />
    </div>
  );
}

export default App;
