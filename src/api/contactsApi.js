import { handleResponse, handleError } from "./apiUtils";
const baseUrl = process.env.REACT_APP_API_URL + "/contacts/";

export function getContacts() {
  return fetch(baseUrl)
    .then(handleResponse)
    .catch(handleError);
}

export function getContactById(id) {
  return fetch(baseUrl + "?id=" + id)
    .then(response => {
      if (!response.ok) throw new Error("Network response was not ok.");
      return response.json().then(contacts => {
        if (contacts.length !== 1) throw new Error("Contact not found: " + id);
        return contacts[0]; // should only find one course for a given slug, so return it.
      });
    })
    .catch(handleError);
}

export function saveContact(contact) {
  return fetch(baseUrl + (contact.id || ""), {
    method: contact.id ? "PUT" : "POST", // POST for create, PUT to update when id already exists.
    headers: { "content-type": "application/json" },
    body: JSON.stringify({
      ...contact
    })
  })
    .then(handleResponse)
    .catch(handleError);
}

export function deleteContact(contactId) {
  return fetch(baseUrl + contactId, { method: "DELETE" })
    .then(handleResponse)
    .catch(handleError);
}
